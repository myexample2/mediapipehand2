package com.example.mediapipemultihandstrackingapp;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;

public class CaptureDialog extends Dialog {

    Context mContext;
    ImageView ivCapture;

    public CaptureDialog(@NonNull Context context) {
        super(context);
        mContext = context;
        setContentView(R.layout.dialog_capture);
        ivCapture = findViewById(R.id.ivCapture);

        Glide.with(context).load(R.raw.loading).into(ivCapture);
    }

    public void setImage(Bitmap bitmap) {
        ivCapture.setImageBitmap(bitmap);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Glide.with(mContext).clear(ivCapture);
    }
}
