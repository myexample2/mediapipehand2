package com.example.mediapipemultihandstrackingapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.util.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.mediapipemultihandstrackingapp.CaptureDialog;
import com.example.mediapipemultihandstrackingapp.R;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmark;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmarkList;
import com.google.mediapipe.components.CameraHelper;
import com.google.mediapipe.components.CameraXPreviewHelper;
import com.google.mediapipe.components.ExternalTextureConverter;
import com.google.mediapipe.components.FrameProcessor;
import com.google.mediapipe.components.PermissionHelper;
import com.google.mediapipe.framework.AndroidAssetUtil;
import com.google.mediapipe.framework.AndroidPacketCreator;
import com.google.mediapipe.framework.PacketGetter;
import com.google.mediapipe.framework.Packet;
import com.google.mediapipe.glutil.EglManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main activity of MediaPipe example apps.
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private static final String BINARY_GRAPH_NAME = "hand_tracking_mobile_gpu.binarypb";
    private static final String INPUT_VIDEO_STREAM_NAME = "input_video";
    private static final String OUTPUT_VIDEO_STREAM_NAME = "output_video";
    private static final String OUTPUT_LANDMARKS_STREAM_NAME = "hand_landmarks";
    private static final String INPUT_NUM_HANDS_SIDE_PACKET_NAME = "num_hands";
    private static final int NUM_HANDS = 1;
    private static final CameraHelper.CameraFacing CAMERA_FACING = CameraHelper.CameraFacing.BACK;
    // Flips the camera-preview frames vertically before sending them into FrameProcessor to be
    // processed in a MediaPipe graph, and flips the processed frames back when they are displayed.
    // This is needed because OpenGL represents images assuming the image origin is at the bottom-left
    // corner, whereas MediaPipe in general assumes the image origin is at top-left.
    private static final boolean FLIP_FRAMES_VERTICALLY = true;

    private static final int WRIST = 0;
    private static final int THUMB_1 = 1;
    private static final int THUMB_2 = 2;
    private static final int THUMB_3 = 3;
    private static final int THUMB_4 = 4; //  엄지
    private static final int INDEX_FINGER_1 = 5;
    private static final int INDEX_FINGER_2 = 6;
    private static final int INDEX_FINGER_3 = 7;
    private static final int INDEX_FINGER_4 = 8; // 검지
    private static final int MIDDLE_FINGER_1 = 9;
    private static final int MIDDLE_FINGER_2 = 10;
    private static final int MIDDLE_FINGER_3 = 11;
    private static final int MIDDLE_FINGER_4 = 12; // 중지
    private static final int RING_FINGER_1 = 13;
    private static final int RING_FINGER_2 = 14;
    private static final int RING_FINGER_3 = 15;
    private static final int RING_FINGER_4 = 16; // 약지
    private static final int PINKY_1 = 17;
    private static final int PINKY_2 = 18;
    private static final int PINKY_3 = 19;
    private static final int PINKY_4 = 20; // 새끼

    static {
        // Load all native libraries needed by the app.
        System.loadLibrary("mediapipe_jni");
        System.loadLibrary("opencv_java3");
    }

    // {@link SurfaceTexture} where the camera-preview frames can be accessed.
    private SurfaceTexture previewFrameTexture;
    // {@link SurfaceView} that displays the camera-preview frames processed by a MediaPipe graph.
    private SurfaceView previewDisplayView;
    // Creates and manages an {@link EGLContext}.
    private EglManager eglManager;
    // Sends camera-preview frames into a MediaPipe graph for processing, and displays the processed
    // frames onto a {@link Surface}.
    private FrameProcessor processor;
    // Converts the GL_TEXTURE_EXTERNAL_OES texture from Android camera into a regular texture to be
    // consumed by {@link FrameProcessor} and the underlying MediaPipe graph.
    private ExternalTextureConverter converter;
    // ApplicationInfo for retrieving metadata defined in the manifest.
    private ApplicationInfo applicationInfo;
    // Handles camera access via the {@link CameraX} Jetpack support library.
    private CameraXPreviewHelper cameraHelper;

    // 손가락 접힘 여부
    boolean isFoldIndexFinger = false;  // 검지
    boolean isFoldMidderFinger = false; // 중지
    boolean isFoldFinger1 = false; // 1번쨰
    boolean isFoldFinger4 = false; // 4번째
    boolean isFoldFinger5 = false; // 5번쨰

    long mClickTime;

    Button btnIndexFingerPoint, btnThubmFingerPoint;    //  클릭 포인트 버튼

    Button btnBack; // 배경

    Button btnArray[] = new Button[2]; //  버튼 1, 2

    boolean mIsPreventContinuousClick = false;  // 연속 클릭 방지

    float preDistanceX = 0;
    float mZoomLevel = 1;

    // 캡쳐 이미지 다이얼로그
    CaptureDialog mCaptureDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 카메라 프리뷰를  전체화면으로 보여주기 위해 셋팅한다.
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setContentView(getContentViewLayoutResId());
        btnIndexFingerPoint = findViewById(R.id.btnIndexFingerPoint);
        btnThubmFingerPoint = findViewById(R.id.btnThubmFingerPoint);

        btnArray[0] = findViewById(R.id.btn1);
        btnArray[1] = findViewById(R.id.btn2);

        btnBack = findViewById(R.id.btnBack);
        try {
            applicationInfo =
                    getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Cannot find application info: " + e);
        }

        previewDisplayView = new SurfaceView(this);
        setupPreviewDisplayView();

        // Initialize asset manager so that MediaPipe native libraries can access the app assets, e.g.,
        // binary graphs.
        AndroidAssetUtil.initializeNativeAssetManager(this);
        eglManager = new EglManager(null);
        processor =
                new FrameProcessor(
                        this,
                        eglManager.getNativeContext(),
                        BINARY_GRAPH_NAME,
                        INPUT_VIDEO_STREAM_NAME,
                        OUTPUT_VIDEO_STREAM_NAME);
        processor
                .getVideoSurfaceOutput()
                .setFlipY(FLIP_FRAMES_VERTICALLY);

        PermissionHelper.checkAndRequestCameraPermissions(this);
        AndroidPacketCreator packetCreator = processor.getPacketCreator();
        Map<String, Packet> inputSidePackets = new HashMap<>();
        inputSidePackets.put(INPUT_NUM_HANDS_SIDE_PACKET_NAME, packetCreator.createInt32(NUM_HANDS));
        processor.setInputSidePackets(inputSidePackets);

        // To show verbose logging, run:
        // adb shell setprop log.tag.MainActivity VERBOSE

        processor.addPacketCallback(
                OUTPUT_LANDMARKS_STREAM_NAME,
                (packet) -> {
                    Log.v(TAG, "Received multi-hand landmarks packet.");
                    List<NormalizedLandmarkList> multiHandLandmarks =
                            PacketGetter.getProtoVector(packet, NormalizedLandmarkList.parser());

                    NormalizedLandmarkList e = multiHandLandmarks.get(0);

                    float standard = e.getLandmark(WRIST).getY(); // 손마디 Y 좌표

                    float thumb4_y_distance = e.getLandmark(THUMB_4).getY() - standard; //엄지 와 손 마디 X거리
                    float thumb2_distance = e.getLandmark(THUMB_2).getY() - standard;

                    float index4_y_distance = e.getLandmark(INDEX_FINGER_4).getY() - standard; //엄지 와 손 마디 X거리
                    float index2_distance = e.getLandmark(INDEX_FINGER_2).getY() - standard;

                    float midel4_y_distance = e.getLandmark(MIDDLE_FINGER_4).getY() - standard; //중지4 와 손 마디 X거리
                    float midel2_y_distance = e.getLandmark(MIDDLE_FINGER_2).getY() - standard;

                    float ring4_y_distance = e.getLandmark(RING_FINGER_4).getY() - standard;
                    float ring2_y_distance = e.getLandmark(RING_FINGER_2).getY() - standard;

                    float pinkey4_y_distance = e.getLandmark(PINKY_4).getY() - standard;
                    float pinkey2_y_distance = e.getLandmark(PINKY_2).getY() - standard;

                    boolean isChangeGesture = false;
                    if ((midel4_y_distance > midel2_y_distance) &&
                            (ring4_y_distance > ring2_y_distance) && (pinkey4_y_distance > pinkey2_y_distance)) { // 3,4,5 번 손가락 모두 접힌 경우
//                    Log.d(TAG, "# 3,4,5 번째 손가락  모두 접힘");
                        if (thumb4_y_distance < thumb2_distance && index4_y_distance < index2_distance ) {
                            // 엄지와 검지는 펴진 상태인 경우
//                        Log.d(TAG, "# 1,2 번째 손가락만 펴짐");
                            isChangeGesture = true;
                            float thubm4_x = e.getLandmark(THUMB_4).getX() - standard;
                            float index4_x = e.getLandmark(INDEX_FINGER_4).getX() - standard;

                            float curDistance = thubm4_x - index4_x;
//                        Log.d(TAG, "#ring2_y_distance : " + midel2_y_distance);
//                        Log.d(TAG, "## curDistance : " + curDistance);
//                        Log.d(TAG, "## preDistanceX : " + preDistanceX);

                            float xDistance = curDistance - preDistanceX;

//                        Log.d(TAG, "## xDistance : " + xDistance);

                            if (preDistanceX == -1) { // 모션 감지 아닌 경우 1회 무시
                                preDistanceX = curDistance;
                            } else {
                                if (xDistance > -0.01 && xDistance < 0.01) { // 값의 차이가 0.01 ~ -0.01 인 경우
                                    Log.d(TAG, "## 현상 유지");
                                } else if (xDistance < 0.01) {
                                    Log.d(TAG, "## 거리 좁아짐");
                                    preDistanceX = curDistance;
                                    mZoomLevel += 0.1;
                                    zoomView(mZoomLevel);
                                } else if (xDistance > -0.01){
                                    Log.d(TAG, "## 거리 넓어짐");
                                    preDistanceX = curDistance;
                                    mZoomLevel -= 0.1;
                                    if (mZoomLevel > 1)   mZoomLevel = 1;
                                    zoomView(mZoomLevel);
                                }
                            }
                        } else {
                            if (mIsPreventContinuousClick) return;
                            Log.v(TAG, "모두 접힘");
                            try {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mIsPreventContinuousClick = true;
                                        new Handler().postDelayed(() -> {
                                            mIsPreventContinuousClick = false;
                                        }, 2000);

                                        if (mCaptureDialog != null && mCaptureDialog.isShowing()) {
                                            mCaptureDialog.dismiss();

                                        } else {
                                            mCaptureDialog = new CaptureDialog(MainActivity.this);
                                            mCaptureDialog.show();
                                        }

                                        mZoomLevel = 1;
                                        zoomView(mZoomLevel);
                                    }
                                });

                            } catch (Exception err) {
                                Log.e("TAG", err.getMessage());

                            }
                        }
                    }

                    if (!isChangeGesture)   preDistanceX = -1;

                    NormalizedLandmark thumbFinger4 = e.getLandmark(THUMB_4);  // 엄지 손가락 4번째 마디
                    NormalizedLandmark thumbFinger3 = e.getLandmark(THUMB_3);  // 엄지 손가락 4번째 마디
                    NormalizedLandmark thumbFinger2 = e.getLandmark(THUMB_2);  // 엄지 손가락 4번째 마디

                    NormalizedLandmark indexFinger4 = e.getLandmark(INDEX_FINGER_4);  // 중지 손가락 4번째 마디
                    NormalizedLandmark indexFinger3 = e.getLandmark(INDEX_FINGER_3);  // 중지 손가락 3번째 마디
                    NormalizedLandmark indexFinger2 = e.getLandmark(INDEX_FINGER_2);  // 중지 손가락 2번째 마디

                    NormalizedLandmark middleFinger4 = e.getLandmark(MIDDLE_FINGER_4);  // 중지 손가락 4번째 마디
                    NormalizedLandmark middleFinger3 = e.getLandmark(MIDDLE_FINGER_3);  // 중지 손가락 3번째 마디
                    NormalizedLandmark middleFinger2 = e.getLandmark(MIDDLE_FINGER_2);  // 중지 손가락 2번째 마디

                    NormalizedLandmark ringFinger4 = e.getLandmark(RING_FINGER_4);  // 엄지 손가락 4번째 마디
                    NormalizedLandmark ringFinger3 = e.getLandmark(RING_FINGER_3);  // 엄지 손가락 4번째 마디
                    NormalizedLandmark ringFinger2 = e.getLandmark(RING_FINGER_2);  // 엄지 손가락 4번째 마디

                    NormalizedLandmark pinkyFinger4 = e.getLandmark(PINKY_4);  // 엄지 손가락 4번째 마디
                    NormalizedLandmark pinkyFinger3 = e.getLandmark(PINKY_3);  // 엄지 손가락 4번째 마디
                    NormalizedLandmark pinkyFinger2 = e.getLandmark(PINKY_2);  // 엄지 손가락 4번째 마디

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //소스
                            btnIndexFingerPoint.setX(indexFinger4.getX() * previewDisplayView.getWidth());
                            btnIndexFingerPoint.setY(indexFinger4.getY() * previewDisplayView.getHeight());

                            btnThubmFingerPoint.setX(thumbFinger4.getX() * previewDisplayView.getWidth());
                            btnThubmFingerPoint.setY(thumbFinger4.getY() * previewDisplayView.getHeight());
                        }
                    });

                    clickUIProcess();

                    if (thumbFinger4.getY() > thumbFinger2.getY() && thumbFinger3.getY() > thumbFinger2.getY()) {
                        // 엄지 접힘
                        isFoldFinger1 = true;
                    } else {
                        isFoldFinger1 = false;

                    }

                    if (middleFinger4.getY() > middleFinger2.getY() && middleFinger3.getY() > middleFinger2.getY()) {
                        // 중지 접힘
                        isFoldMidderFinger = true;
                    } else {
                        isFoldMidderFinger = false;
                    }


                    if (indexFinger4.getY() > indexFinger2.getY() && indexFinger3.getY() > indexFinger2.getY()) {
                        // 검지 접힘
                        isFoldIndexFinger = true;
                    } else {
                        isFoldIndexFinger = false;
                    }

                    if (ringFinger4.getY() > ringFinger2.getY() && ringFinger3.getY() > ringFinger2.getY()) {
                        // 4번째 손가락 접힘
                        isFoldFinger4 = true;
                    } else {
                        isFoldFinger4 = false;
                    }

                    if (pinkyFinger4.getY() > pinkyFinger2.getY() && pinkyFinger3.getY() > pinkyFinger2.getY()) {
                        // 약지 접힘
                        isFoldFinger5 = true;
                    } else {
                        isFoldFinger5 = false;
                    }

//                    Log.v(
//                            TAG,
//                            "[TS:"
//                                    + packet.getTimestamp()
//                                    + "] "
//                                    + getMultiHandLandmarksDebugString(multiHandLandmarks));
                });

        new Handler().postDelayed(() -> {

        }, 3000);
    }

    public void zoomView(float zoomLevel) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //소스
                findViewById(R.id.preview_display_layout).setScaleX(zoomLevel);
                findViewById(R.id.preview_display_layout).setScaleY(zoomLevel);
            }
        });

    }

    public static Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        if (view instanceof SurfaceView) {
            SurfaceView surfaceView = (SurfaceView) view;
            surfaceView.setZOrderOnTop(true);
            surfaceView.draw(canvas);
            surfaceView.setZOrderOnTop(false);
            return bitmap;
        } else {

    //For ViewGroup & View
            view.draw(canvas);
            return bitmap;
        }
    }

    public void btnClick(View v) {
        switch (v.getId()) {
            case R.id.btn1:
                btnBack.setVisibility(View.GONE);
//                Toast.makeText(getApplicationContext(), "btn [1] click!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn2:
                btnBack.setVisibility(View.VISIBLE);
//                Toast.makeText(getApplicationContext(), "btn [2] click!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void clickUIProcess() {
        if (mIsPreventContinuousClick) return;  // 연속 클릭시 클릭 처리 하지않음

        // 검지 X,Y 포인트 좌표
        int indexFinPointX = (int) (btnIndexFingerPoint.getX());
        int indexFinPointY = (int) (btnIndexFingerPoint.getY());

        // 엄지 X,Y 포인트 좌표
        int thumbFinPointX = (int) (btnThubmFingerPoint.getX());
        int thumbFinPointY = (int) (btnThubmFingerPoint.getY());

        for (Button btn : btnArray) {
            // 버튼 Area 클릭 여부 확인
            if (btn.getX() <= indexFinPointX && (btn.getX() + btn.getWidth()) >= indexFinPointX
                    && btn.getY() <= indexFinPointY && (btn.getY() + btn.getHeight() >= indexFinPointY)) {

                if (btn.getX() <= thumbFinPointX && (btn.getX() + btn.getWidth()) >= thumbFinPointX
                        && btn.getY() <= thumbFinPointY && (btn.getY() + btn.getHeight() >= thumbFinPointY)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btn.performClick();

                            mIsPreventContinuousClick = true;
                            new Handler().postDelayed(() -> {
                                mIsPreventContinuousClick = false;
                            }, 2000);

                        }
                    });
                }
            }
        }

        //                    int finglePixelX = (int) (indexFingle4.getX() * previewDisplayView.getWidth());
//                    int finglePixelY = (int) (indexFingle4.getY() * previewDisplayView.getHeight());
////                    Log.v(TAG, "x : " + indexFingle.getX() + "y : " + indexFingle.getY() /* + "z : " + indexFingle.getZ()*/);
//                    Log.v(TAG, "finglePixelX : " + finglePixelX + "finglePixelY : " + finglePixelY);
//
//                    Log.v(TAG, "finglePixelXpreviewDisplayView.getWidth() : " + previewDisplayView.getWidth() + "previewDisplayView.getHeight() : " + previewDisplayView.getHeight());
//                    Log.v(TAG, "btn1.getX() : " + btn1.getX() + "btn1.getY() : " + btn1.getY());
//                    Log.v(TAG, "btn1.getWidth() : " + btn1.getWidth() + "btn1.getHeight() : " + btn1.getHeight());
    }

    // Used to obtain the content view for this application. If you are extending this class, and
    // have a custom layout, override this method and return the custom layout.
    protected int getContentViewLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        converter =
                new ExternalTextureConverter(
                        eglManager.getContext(), 2);
        converter.setFlipY(FLIP_FRAMES_VERTICALLY);
        converter.setConsumer(processor);
        if (PermissionHelper.cameraPermissionsGranted(this)) {
            startCamera();
        }

        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(manager.getCameraIdList()[0]);
            float maxzoom = (characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM)) * 10;

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    //화면 캡쳐하기
    public File ScreenShot(View view) {
        view.setDrawingCacheEnabled(true);  //화면에 뿌릴때 캐시를 사용하게 한다

        Bitmap screenBitmap = view.getDrawingCache();   //캐시를 비트맵으로 변환
        Log.v(TAG, "# screenBitmap width : " + screenBitmap.getWidth());

        mCaptureDialog = new CaptureDialog(MainActivity.this);
        mCaptureDialog.setImage(screenBitmap);
        mCaptureDialog.show();

        return null;
//        String filename = "screenshot.png";
//        File file = new File(Environment.getExternalStorageDirectory()+"/Pictures", filename);  //Pictures폴더 screenshot.png 파일
//        FileOutputStream os = null;
//        try{
//            os = new FileOutputStream(file);
//            screenBitmap.compress(Bitmap.CompressFormat.PNG, 90, os);   //비트맵을 PNG파일로 변환
//            os.close();
//        }catch (IOException e){
//            e.printStackTrace();
//            return null;
//        }
//
//        view.setDrawingCacheEnabled(false);
//        return file;
    }


    @Override
    protected void onPause() {
        super.onPause();
        converter.close();

        // Hide preview display until we re-open the camera again.
//        previewDisplayView.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    protected void onCameraStarted(SurfaceTexture surfaceTexture) {
        previewFrameTexture = surfaceTexture;
        // Make the display view visible to start showing the preview. This triggers the
        // SurfaceHolder.Callback added to (the holder of) previewDisplayView.
        previewDisplayView.setVisibility(View.VISIBLE);
    }

    protected Size cameraTargetResolution() {

        if (Build.MODEL.equals("T1100G")) { // Note -  Real Wear인 경우 이유는 모르겠으나 size를 낮추는 경우 카메라 표시되는 view 면적이 넓어짐 (실 테스트를 통한 수치 가져와야함)
            return new Size(200, 200); // Size Setting
        } else {
            return null; // No preference and let the camera (helper) decide.
        }
    }

    public void startCamera() {
        cameraHelper = new CameraXPreviewHelper();
        cameraHelper.setOnCameraStartedListener(
                surfaceTexture -> {
                    onCameraStarted(surfaceTexture);
                });
        CameraHelper.CameraFacing cameraFacing = CameraHelper.CameraFacing.BACK;
        cameraHelper.startCamera(
                this, cameraFacing, /*unusedSurfaceTexture=*/ null, cameraTargetResolution());
    }

    protected Size computeViewSize(int width, int height) {
        return new Size(width, height);
    }

    protected void onPreviewDisplaySurfaceChanged(
            SurfaceHolder holder, int format, int width, int height) {
        // (Re-)Compute the ideal size of the camera-preview display (the area that the
        // camera-preview frames get rendered onto, potentially with scaling and rotation)
        // based on the size of the SurfaceView that contains the display.
        Size viewSize = computeViewSize(width, height);
        Size displaySize = cameraHelper.computeDisplaySizeFromViewSize(viewSize);
        boolean isCameraRotated = cameraHelper.isCameraRotated();

        // Connect the converter to the camera-preview frames as its input (via
        // previewFrameTexture), and configure the output width and height as the computed
        // display size.
        converter.setSurfaceTextureAndAttachToGLContext(
                previewFrameTexture,
                isCameraRotated ? displaySize.getHeight() : displaySize.getWidth(),
                isCameraRotated ? displaySize.getWidth() : displaySize.getHeight());
    }

    private void setupPreviewDisplayView() {
        previewDisplayView.setVisibility(View.GONE);
        ViewGroup viewGroup = findViewById(R.id.preview_display_layout);
        viewGroup.addView(previewDisplayView);

        previewDisplayView
                .getHolder()
                .addCallback(
                        new SurfaceHolder.Callback() {
                            @Override
                            public void surfaceCreated(SurfaceHolder holder) {
                                processor.getVideoSurfaceOutput().setSurface(holder.getSurface());
                            }

                            @Override
                            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                                onPreviewDisplaySurfaceChanged(holder, format, width, height);
                            }

                            @Override
                            public void surfaceDestroyed(SurfaceHolder holder) {
                                processor.getVideoSurfaceOutput().setSurface(null);
                            }
                        });
    }

    private String getMultiHandLandmarksDebugString(List<NormalizedLandmarkList> multiHandLandmarks) {
        if (multiHandLandmarks.isEmpty()) {
            return "No hand landmarks";
        }
        String multiHandLandmarksStr = "Number of hands detected: " + multiHandLandmarks.size() + "\n";
        int handIndex = 0;
        for (NormalizedLandmarkList landmarks : multiHandLandmarks) {

            multiHandLandmarksStr +=
                    "\t#Hand landmarks for hand[" + handIndex + "]: " + landmarks.getLandmarkCount() + "\n";
            int landmarkIndex = 0;
            for (NormalizedLandmark landmark : landmarks.getLandmarkList()) {
                multiHandLandmarksStr +=
                        "\t\tLandmark ["
                                + landmarkIndex
                                + "]: ("
                                + landmark.getX()
                                + ", "
                                + landmark.getY()
                                + ", "
                                + landmark.getZ()
                                + ")\n";
                ++landmarkIndex;
            }
            ++handIndex;
        }
        return multiHandLandmarksStr;
    }


}